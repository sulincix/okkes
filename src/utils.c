#include <stdlib.h>
#include <string.h>
char cmd[1024]="";
void fail(char *error){
	std::cout<<col_31("Kurulum tamamlanamadı: ")<<col_33(error)<<std::endl;
	if(force==0){exit(1);}
}
int unpack(){
	system("rm -rf /tmp/okkes/ ; mkdir -p /tmp/okkes/");
	system("tar -xvf /tmp/archive.tar.xz -C /tmp/okkes/ | busybox sed \"s/^/\033[32;1mAyıkla:\\\t\033[;0m/g\"");
	return system("rm -f /tmp/arhive.tar.xz");
}

int mkdir(char *dir,char *name){
	strcpy(cmd,"mkdir -p ");
	strcat(cmd,dir);
	strcat(cmd,name);
	return system(cmd);
}
void install(char *dir,char *name,char *type){
	unpack();
	mkdir(dir,name);
	strcpy(cmd,"cp -prfv /tmp/okkes/* ");
	strcat(cmd,dir);
	strcat(cmd,"/ 2> /dev/null | busybox sed \"s/^.*\\//\033[32;1mYükle:\\\t\033[;0m/g\"");
	system(cmd);
	mkdir((char*)"/var/",(char*)"okkes");
	strcpy(cmd,"ls /tmp/okkes/ | busybox sed \"s|^|rm -rf ");
	strcat(cmd,dir);
	strcat(cmd,"|g\" > /var/okkes/");
	strcat(cmd,type);
	strcat(cmd,"_");
	strcat(cmd,name);
	system(cmd);

}
int del_dir(char *type,char *name){
	strcpy(cmd,"busybox [ -f \"/var/okkes/");
	strcat(cmd,type);
	strcat(cmd,"_");
	strcat(cmd,name);
	strcat(cmd,"\" ]");
	if(system(cmd)!=0){fail((char*)"Paket mevcut değil.");}
	strcpy(cmd,"busybox sh /var/okkes/");
	strcat(cmd,type);
	strcat(cmd,"_");
	strcat(cmd,name);
	system(cmd);
	strcpy(cmd,"rm -f /var/okkes/");
	strcat(cmd,type);
	strcat(cmd,"_");
	strcat(cmd,name);
	return system(cmd);
	
}
void help_msg(){
	std::cout<<"\
Ökkeş inary paket sisteminin yardımcısıdır.\n\
"<<col_33("Kullanım: ")<<col_32("okkes\t")<<col_36("[-i/-r/--help]\t")<<col_34("[app/icon/theme/pip/font]\t")<<col_35(" [paket ismi]")<<"\n\
\t"<<col_36("-i\t:")<<"\tPaket kurulumu moduna geçer (varsayılan)\n\
\t"<<col_36("-r\t:")<<"\tPaket Kaldırma moduna geçer.\n\
\t"<<col_36("-f\t:")<<"\tOnayları ve Hataları görmezden gelir. (tehlikeli)\n\
\t"<<col_36("-h\t:")<<"\tBu mesajı görüntüler.\n\
\t"<<col_36("app\t:")<<"\tÖkkeş daha önceden hazırlanmış betikleri kullanarak\n\t\t\ttaşınabilir uygulama kurar.\n\
\t"<<col_34("icon\t:")<<"\tÖkkeş simge temasını indirir ve kurar.\n\
\t"<<col_34("theme\t:")<<"\tÖkkeş sistem temasını indirir ve kurar.\n\
\t"<<col_34("font\t:")<<"\tÖkkeş yazı tipini indirir ve kurar.\n";
if(is_pip_avaible==0){
std::cout<<"\t"<<col_34("pip\t:")<<"\tÖkkeş pip deposundaki python kütüphanesini indirir\n\t\t\tve kurar.\n\n\"";
}
std::cout<<col_33("Önemli not")<<": Ökkeş ile kuracağınız paketler DOĞRULANMAZ. Bu sebeple ökkeşi\nkullanmanızdan gelecek olumsuz durumların sorumluluğu size aittir.\n";

}
int is_file(char *name){
	strcpy(cmd,"[ -f \"");
	strcat(cmd,name);
	strcat(cmd,"\" ]");
	return system(cmd);
}
