#include <vars.h>
void okkes(char*type,char*name){
	if(uninstall==0){
		std::cout<<col_32("Paket: ")<<name<<std::endl<<col_32("Türü: ")<<type<<std::endl;
		if (!strcmp(type,"icon")){
			if(download((char*)iconrepo,name)==0){
				install((char*)icondir,name,type);
			}else{
				fail((char*)"Arşiv indirilemedi.");
			}
		}else if (!strcmp(type,"font")){
			if(download((char*)fontrepo,name)==0){
				install((char*)fontdir,name,type);
			}else{
				fail((char*)"Arşiv indirilemedi.");
			}		
		}else if (!strcmp(type,"theme")){
			if(download((char*)themerepo,name)==0){
				install((char*)themedir,name,type);
			}else{
				fail((char*)"Arşiv indirilemedi.");
			}
		}else if (!strcmp(type,"wallpaper")){
			if(download((char*)wallpaperrepo,name)==0){
				install((char*)themedir,name,type);
			}else{
				fail((char*)"Arşiv indirilemedi.");
			}
		}else if (!strcmp(type,"pip")){
			if(pip(name)!=0){
				fail((char*)"Komut yürütülürken hata oluştu");
			}
		}else if (!strcmp(type,"app")){
			if(run_script(name)!=0){
				fail((char*)"Komut yürütülürken hata oluştu");
			}
		}else if (!strcmp(type,"snap")){
			if(snap_dl(name)!=0){
				fail((char*)"Snap paketi indirilemedi.");
			}if(snap_mount(name)!=0){
				fail((char*)"Snap paketi dizine bağlanamadı.");
			}if(snap_install(name)!=0){
				fail((char*)"Snap paketi kurulamadı.");
			}snap_umount((char*) name);
		}else if (!strcmp(type,"deb")){
			if(unpack_deb(name)!=0){
				fail((char*)"Arşiv ayıklanamadı.");
			}install_deb_package();
		}
		std::cout<<col_32("Tamamlandı:")<<col_33(name)<<std::endl;
	}else{
		if (!strcmp(type,"icon")){
			if(del_dir(type,name)!=0){
				fail((char*)"Silme başarısız.");
			}
		}else if (!strcmp(type,"font")){
			if(del_dir(type,name)!=0){
				fail((char*)"Silme başarısız.");
			}
		}else if (!strcmp(type,"theme")){
			if(del_dir(type,name)!=0){
				fail((char*)"Silme başarısız.");
			}
		}else if (!strcmp(type,"wallpaper")){
			if(del_dir(type,name)!=0){
				fail((char*)"Silme başarısız.");
			}
		}else if (!strcmp(type,"pip")){
			if(pipremove(name)!=0){
				fail((char*)"Komut yürütülürken hata oluştu");
			}
		}else if (!strcmp(type,"app")){
			if(run_script(name)!=0){
				fail((char*)"Komut yürütülürken hata oluştu");
			}
		}else if (!strcmp(type,"snap")){
			if(del_dir(type,name)!=0){
				fail((char*)"Silme başarısız.");
			}
		}else if (!strcmp(type,"deb")){
			if(del_dir(type,name)!=0){
				fail((char*)"Silme başarısız.");
			}
		}
		std::cout<<col_32("Tamamlandı:")<<col_33(name)<<std::endl;

	}
}
