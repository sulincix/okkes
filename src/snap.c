
int snap_dl(char* name){
	strcpy(cmd,"busybox wget -c \"$(curl \"https://search.apps.ubuntu.com/api/v1/search?q=");
	strcat(cmd,name);
	strcat(cmd,"\" | sed \"s|\\\\.snap\\\".*|.snap|g\" | grep \"https://\" | sed \"s|.*\\\"https://|https://|g\" 2> /dev/null)\" -O /tmp/");
	strcat(cmd,name);
	strcat(cmd,".snap");
	return system(cmd);
}

int snap_mount(char* name){
	mkdir((char*)"/tmp/",(char*)"okkes");
	strcpy(cmd,"mount /tmp/");
	strcat(cmd,name);
	strcat(cmd,".snap /tmp/okkes/");
	return system(cmd);
}
void snap_umount(char* name){
	strcpy(cmd,"umount -Rf /tmp/okkes/");
	system(cmd);
	system(cmd);
}

int snap_install(char* name){
	mkdir((char*)"/snap/",name);
	strcpy(cmd,"cp -prfv /tmp/okkes/* /snap/");
	strcat(cmd,name);
	strcat(cmd,"/ 2> /dev/null | busybox sed \"s/^.*\\//\033[32;1mYükle:\\\t\033[;0m/g\"");
	system(cmd);
	strcpy(cmd,"echo \"export SNAP=/snap/");
	strcat(cmd,name);
	strcat(cmd,"\" > /snap/bin/");
	strcat(cmd,name);
	system(cmd);
	strcpy(cmd,"echo \"export SNAP_USER_DATA=\\$HOME/.snap\" >> /snap/bin/");
	strcat(cmd,name);
	system(cmd);
	strcpy(cmd,"echo \"export SNAP_ARCH=amd64\" >> /snap/bin/");
	strcat(cmd,name);
	system(cmd);
	strcpy(cmd,"echo \"export SNAP_USER_COMMON=\\$HOME/.snap\" >> /snap/bin/");
	strcat(cmd,name);
	system(cmd);
	strcpy(cmd,"echo \"export HOME=\\$HOME/.snap\" >> /snap/bin/");
	strcat(cmd,name);
	system(cmd);
	strcpy(cmd,"echo \"export PATH=\\$PATH:\\$SNAP/bin:\\$SNAP/usr/bin\" >> /snap/bin/");
	strcat(cmd,name);
	system(cmd);
	strcpy(cmd,"ls /snap/");
	strcat(cmd,name);
	strcat(cmd,"/ | grep \".wrapper\" | head -n 1  | sed \"s|^|/snap/");
	strcat(cmd,name);
	strcat(cmd,"/|g\">> /snap/bin/");
	strcat(cmd,name);
	system(cmd);
	strcpy(cmd,"chmod +x /snap/bin/");
	strcat(cmd,name);
	system(cmd);
	mkdir((char*)"/var/",(char*)"okkes");
	strcpy(cmd,"echo \"rm -rf /snap/");
	strcat(cmd,name);
	strcat(cmd,"\" > /var/okkes/snap_");
	strcat(cmd,name);
	system(cmd);
	strcpy(cmd,"echo \"rm -rf /snap/bin/");
	strcat(cmd,name);
	strcat(cmd,"\" >> /var/okkes/snap_");
	strcat(cmd,name);
	return system(cmd);
}
