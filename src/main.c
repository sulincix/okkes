#include <iostream>
#include <color.h>
#include <vars.h>
int force=0;
int uninstall=0;
int is_pip_avaible=0;
int mode=0;
int trig_icon=0;
int trig_font=0;
#include <utils.c>
#include <downloader.c>
#include <pip.c>
#include <app.c>
#include <snap.c>
#include <deb.c>
#include <okkes.c>
int main(int argc, char *argv[]){
	if(system("busybox [ \"$USER\" == \"root\" ]")!=0){fail((char*)"Erişim engellendi.");}
	is_pip_avaible=system("[ -f \"/usr/bin/pip\" ]");
	if(is_pip_avaible!=0){
		std::cout<<col_31("Pip modülü devre dışı!!!")<<std::endl;
	}
	if(argc<=2){
		help_msg();
	}
	for(int i=1;i<argc;i++){
		if(!strcmp(argv[i],"icon")){
			mode=1;
		}else if(!strcmp(argv[i],"theme")){
			mode=2;
		}else if(!strcmp(argv[i],"font")){
			mode=3;
		}else if(!strcmp(argv[i],"pip")){
			mode=4;
		}else if(!strcmp(argv[i],"app")){
			mode=5;
		}else if(!strcmp(argv[i],"wallpaper")){
			mode=6;
		}else if(!strcmp(argv[i],"snap")){
			mode=7;
		}else if(!strcmp(argv[i],"deb")){
			mode=8;
		}else if(!strcmp(argv[i],"-f")){
			force=1;
		}else if(!strcmp(argv[i],"-r")){
			uninstall=1;
		}else if(!strcmp(argv[i],"-i")){
			uninstall=0;
		}else if(!strcmp(argv[i],"-h")){
			help_msg();
		}else{
			if(mode==1){
				trig_icon=1;
				okkes((char*)"icon",argv[i]);
			}else if(mode==2){
				okkes((char*)"theme",argv[i]);
			}else if(mode==3){
				trig_font=1;
				okkes((char*)"font",argv[i]);
			}else if(mode==4 && is_pip_avaible==0){
				okkes((char*)"pip",argv[i]);
			}else if(mode==5){
				okkes((char*)"app",argv[i]);
			}else if(mode==6){
				okkes((char*)"wallpaper",argv[i]);
			}else if(mode==7){
				okkes((char*)"snap",argv[i]);
			}else if(mode==8){
				okkes((char*)"deb",argv[i]);
			}else{
				fail((char*)"Geçersiz parametre");			
			}
		}
	if(trig_font==1){
		system(fonttriger);
	}else if(trig_icon==1){
		system(icontriger);
	}
	}
	return 0;
}
