BINDIR=/usr/bin/
CC=g++
INC=-I src
OPT=-o
SRCS=src/main.c
OUT=build/okkes
build:
	mkdir build
	$(BINDIR)$(CC) $(INC) $(OPT) $(OUT) $(SRCS)
clean:
	rm -rf build
update-git:
	#bura sırf geliştiricinin işini kolaylaştırmak için. takmayın :D
	git add .
	git commit -m "hebele hübele"
	git push -u origin master
install:
	install build/okkes $(PREFIX)/usr/bin/okkes
all: clean build
